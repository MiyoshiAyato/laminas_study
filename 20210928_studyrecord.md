# 学習記録(日付)
## 本日のTODO
* LaminasのMVC学習
* Module Managerについての学習
  * Module Class
<br>

## 本日のTODOの達成率とその理由
* 本日のTODOの達成率:**100%**
<br>

## 本日学んだこと / 振り返り
* `laminas-modulemanager`とはアプリケーションが使用するモジュールを指定することができ、各モジュールを探し出して初期化するコンポーネント。
<br>
* モジュール
  * モジュール名を区切る場合は、小文字のハイフンで区切った**スパイナルケース**にする。
  ```php
  "LaminasUser "  //×
  "laminas-user "  //○
  ```
  <br>
* コントローラー
  * `LaminasdhouctionControllerAbstractActionController`では、ルートが「アクション」にマッチすることを許可し、アクションにマッチした場合、そのアクションにちなんだメソッドがコントローラから呼び出されます。特定のアクションへのURLのマッピングは、モジュールの`module.config.php`ファイルで定義されているルートを使用して行われます。例えば、`action`キーに `foo`を返すルートがあった場合、`fooAction`メソッドが呼び出されます。
  (チュートリアルでも、`AbstractActionController`を使用。上記例のように、`indexAction`や`addAction`を定義しました。)
  * `LaminasumvcdhoreContrallionAbstractRestfulController`は、Requestを見て、どのようなHTTPメソッドが使われたかを判断し、それに応じたメソッドを呼び出す。
GETの場合は、`getList()`メソッド、ルーティング時に「id」がマッチした場合は、`get()`メソッドが呼び出されます。
POSTでは、$_POSTの値を渡して`create()`メソッドを呼び出します。
PUTは、ルーティング時に "id" がマッチすることを想定していて、idとデータを渡して`update()`メソッドを呼び出します。
DELETEでは、ルーティングの際に「id」がマッチした場合`delete()`メソッドが呼び出されます。
<br>

## 困りごと / 質問など
* いつも質問に回答いただきありがとうございます！  
  今回は特にありません。

## 明日のTODO
* LaminasのMVC学習
  * ビューやルートから

* Module Managerについての学習
  * Module Autoloaderについて
