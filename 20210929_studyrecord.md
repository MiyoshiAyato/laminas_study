# 学習記録(2021/9/29)
## 本日のTODO
* LaminasのMVC学習
  * ビュー
  * ルート
* Module Managerについての学習
  * Module Autoloaderについて
<br>

## 本日のTODOの達成率とその理由
* 本日のTODOの達成率:**100%**
<br>

## 本日学んだこと / 振り返り
* `laminas-modulemanager`には、デフォルトでモジュールオートローダを使用。(vender配下のLaminas\ModuleAutoloader)これにより、様々なソースからModuleクラスを探し出してくれる。
<br>
* オートローダ使用方法は、laminas-loaderコンポーネントでオートローディング実行する方法がひとつ。こちらは、`public/index.php`ファイルの設定をする。
一方、オートロードにComposerを使用する方法も。こちらのやり方は、チュートリアルでもやりましたが、`composer.json`を開き、`autoload`セクションを設定。公式でもComposerのオートローディング機能を使うことを推奨しており、自分としてもそちらの方が使いやすく感じました。
<br>
* `ModuleAutoloader`の無効化が必要。
  * オートロードにComposerを使用している場合、`ModuleAutoloader`が必要ないため無効化する。(チュートリアル ではやってなかった...)
  `config/application.config.php`ファイルの`module_listener_options`コンフィギュレーション内に下記を記述
    ```php
    'use_laminas_loader' => false
    ```
<br>

## 困りごと / 質問など
* いつも質問に回答いただきありがとうございます！  
  今回は特にありません。
<br>

## 明日のTODO
* Module Managerについて
  * サービスマネージャについて
  * プラグインマネージャーについて
  * ViewManagerについて
* Laminas MVC
  * モジュールを作成する際のベストプラクティス
