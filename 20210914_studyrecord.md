# 学習記録(2021/9/14)

## 行ったこと
* Webサーバーのセットアップ
  * docker-compose経由

* dockerについて復習
  * GIZTECHの『Docker 仮想環境構築』にて復習

* docker-composeでのWebサーバーのセットアップ時のエラー対応

* 開発モードの設定

* アルバムモジュールの設定

## 学んだこと
* GIZTECHの『Docker 仮想環境構築』にて復習。
`docker-compose up -d`でエラーが多々出ましたが、
原因は、別のディレクトリで`docker-compose up -d`を起動したままであったり、
インストールしたLaminasチュートリアルのディレクトリ階層がが今までのLaravelとは違うためそこで躓いてしまいました。コマンドを叩く階層を今までのLaravelと同じ感覚でやらないよう注意しなければいけないと感じました。
<br>
* 開発モード(デバッグモード)の切り替えは、Laravelの.envを編集するのとは違い、コマンドで切り替えが出来ました。
コマンドにて切り替えた際、
```php
> laminas-development-mode enable
Already in development mode!
```
と表示されたため、デフォルトでデバッグモードがONになっているところは、Laravel同様同じでした。

* Laminasは、モジュールシステムを使用して、各モジュール内にメインのアプリケーション固有のコード(今回は、コントローラ、モデル、フォーム、ビュー、設定ファイル)を整理していることがわかりました。
LaminasのModuleManager機能を使用して、モジュールをロードして設定するみたいですが、明日該当の`Module.php`を編集するところから始めたいと思います。