# 学習記録(2021/9/17)

## 行ったこと
* SQLスキーマファイルを使用してデータベースにデータを追加
* モデルファイルの編集
* モジュールファイル編集
* コントロールファイルの編集
* 一覧ページのview作成　

## 学んだこと
* PHPを使用したデータベースの作成が可能なこと
<br>
* ViewModelという概念
  * Model から取得したデータを View 用に整形したり書式設定したりできる
  * Model と View の間のやりとりがシンプルになる
  * View をドメインモデルと切り離して検証できる
<br>
* Laminasでは、2つのコントローラ抽象クラスが用意されている
  * `LaminasdousthController_AbstractActionController`
  * `LaminasdousthController_AbstractRestfulController`
今回は、標準的な`AbstractActionController`を使用しましたが、RESTfulなWebサービスを書こうとしている場合は`AbstractRestfulController`が便利だそうです。
<br>
* viewの作成
laminas-mvcとlaminas-viewによって提供されているもの
  * `headTitle()`・・・<head> セクションのタイトルを設定
  * `url()`・・・リンクを作成するために使用
  * `escapeHtml()`・・・クロスサイトスクリプティング（XSS）の脆弱性から対策するときに使用
