# 学習記録(2021/9/17)

## 行ったこと
* 編集機能
  * コントローラにてeditActionの追加
  * ModelのAlbumクラスにメソッドの追加
  * 編集画面の追加
* 削除機能(途中)
  * コントローラにてdeleteActionの追加
<br>
<br>

## 学んだこと
* headセクションのタイトルは、headTitle() ビューヘルパーを使用
  ```php
  //例
  $title = 'Edit album';
  $this->headTitle($title);
  ```
<br>

* Laminasのform()ビューヘルパーにはopenTag()とcloseTag()メソッドがあり、これを使ってフォームを開いたり閉じたりする。
  ```php
  //Laminasの場合
  echo $this->form()->openTag();
  echo $this->form()->closeTag();

  //Laravelの場合
  {{ Form::open([]) }}
  {{ Form::close() }}
  ```
<br>

* form()ビューヘルパにてtypeの指定を行う。
  ```php
  //例
  echo $this->formSubmit($submit);
  echo $this->formHidden($form->get('id'));
  ```
<br>

* 編集機能にてフォームを表示する際に、`hydrator object`を使って、各要素の初期値をモデルから引っ張ってくることが可能。
hydratorには様々な種類があるが、デフォルトはLaminas\Hydrator\ArraySerializableで、モデルの中に`getArrayCopy()`をオーバーライドさせる。`getArrayCopy()`にて各要素を定義するとそこを初期値として引っ張ってくることでき、フォームに編集前の値を表示することができる。
<br>
<br>

## 次回やること
* 削除機能の実装(ユーザーが削除をクリックしたときに確認フォームを表示し、「はい」をクリックしたら削除を実行するように。)
  * ユーザーに確認メッセージを表示し、「Yes」と「No」のボタンを持つフォームを表示する画面の作成

* アルバムのリストが表示されるようにする


