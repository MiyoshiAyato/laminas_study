# 学習記録(2021/9/17)

## 行ったこと
* 新規作成機能
  * 新しいアルバムの追加
  * Albumクラスに機能の追加
  * コントローラにてaddActionの追加
  * 新規作成画面の追加
<br>
<br>

## 学んだこと
* ユーザーが詳細を入力するためのフォームを表示するのに、`laminas-form`にて様々なフォーム入力とその検証を行う。
<br>
* `laminas-inputfilter`コンポーネントによって,
フォームの送信を処理し、データベースに保存する。
<br>
* HTMLフォームの送信については、laminas-formのデフォルトはPOST。GETに変更したい場合は、コンストラクタでmethod属性を設定。
  ```php
  //例
  $this->setAttribute('method', 'GET');
  ```
<br>
<br>

* フィルタリングやバリデーション
  ```php
  // titleプロパティの例
  $inputFilter->add([
      'name' => 'title',
      'required' => true,
      'filters' => [
          ['name' => StripTags::class],
          ['name' => StringTrim::class],
      ],
      'validators' => [
          [
              'name' => StringLength::class,
              'options' => [
                  'encoding' => 'UTF-8',
                  'min' => 1,
                  'max' => 100,
              ],
          ],
      ],
  ]);
  ```
  必須に設定し、テキスト要素には、`filters`にて`StripTags`と`StringTrim`という2つのフィルタを追加し、不要なHTMLや不要なホワイトスペースを取り除くことが可能。また、`StringLength`バリデーターからoptionsにより文字数制限が可能。
  これらのフィルタリングやバリデーションは、ModelのAlbumクラスに機能追加して記述。
<br>
* viewについては後日、記述いたします。
<br>
<br>

## わからないこと
* インターフェースを実装したクラスがあり、implementsがわからず、  
[PHP インターフェースのサンプル](https://itsakura.com/php-interface)  
上記サイトにて調べてみました。  
インスタンス化できないもので、宣言すれば記述量が増えるだけだと感じており、interfaceの使い道や利点がよくわからずでした。
<br>
<br>

## 明日やること
* アルバムの編集機能及び削除機能。



