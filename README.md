# 学習記録(2021/9/13)
## 行ったこと
下記コマンドにより、プロジェクトの作成。
```php
$ composer create-project -s dev laminas/laminas-mvc-skeleton path/to/install
```  
これにより、次のような依存関係の初期セットがインストールされます。
* laminas-component-installer(アプリケーションへのコンポーネント構成の挿入を自動化するのに役立つ。)
* laminas-mvc(MVCアプリケーションのカーネル。)
<br>


インストール時に下記のプロンプトが表示されたため、
[Laminas Documentation](https://docs.laminas.dev/tutorials/getting-started/skeleton-application/)
こちらの通り、インストーラーによって発行されるすべてのプロンプトでのオプションを指定しました。
<br>
しかし、インストール時、下記のエラーが発生しました。
```php
[Composer\Downloader\TransportException]
  The "https://packagist.org/p/symfony/polyfill-ctype%244db31e7a9da37d55222ba37804135b9fba8747ab1a48297335fd322ea855a827.json" file could not be downloaded (HTTP/1.1 404 Not Found)
```
**原因調査**
何が原因なのかを、`$composer diag` で調べました。
すると、composer versionのことで言われているので、
```php
Checking composer version: You are not running the latest stable version, run `composer self-update` to update (1.10.22 => 2.1.6)
```
下記コマンドにて、composerのversionをアップデートしました。  
`$composer self-update --2`  
アップデート後、再度プロジェクトの作成を行い
>* laminas-component-installer(アプリケーションへのコンポーネント構成の挿入を自動化するのに役立つ。)
>* laminas-mvc(MVCアプリケーションのカーネル。)

上記初期セットのインストールまで完了いたしました。
