# 学習記録(日付)

## 本日のTODO
* 削除機能の実装
* アルバムのリスト表示
* 一緒に参画する先輩社員や営業の方との相談
* [Documentation](https://docs.laminas.dev/laminas-mvc/intro/)にあるlaminas-mvcの学習

## 本日のTODOの達成率とその理由
* 本日のTODOの達成率:**100%**
* 理由
削除機能の前半は、編集機能と同じ流れだったためです。(マッチしたルートから`id`を取得し、 リクエストオブジェクトの`isPost()`をチェックして、 確認ページを表示する)ただ、英語の翻訳に時間を使っており、翻訳回数をなるべく減らしていくのを今後の目標にしたいです。

## 本日学んだこと / 振り返り
* 確認画面を挟んだ、削除機能の実装
* ホームページにてアルバムの一覧を表示するには、作成した`Albumモジュール`ではなく、`Applicationモジュール`のmodule.config.phpで設定されているルートを変更。(作成したAlbumControllerをインポートして、既存の`controller`の値を設定)

## 困りごと / 質問など
* 今回のチュートリアルでは、下記のように型を記述して実装しました。このような書き方は他でもするのでしょうか？(型の部分を削除しても挙動は変わりませんでした。)
  ```php
  //例
  ///(int)にて型の表示
  <input type="hidden" name="id" value="<?= (int) $album->id ?>" />

  $id = (int) $this->params()->fromRoute('id', 0);
  ```

## 明日のTODO
* [Documentation](https://docs.laminas.dev/laminas-mvc/intro/)にあるlaminas-mvcの学習