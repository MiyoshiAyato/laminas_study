# 学習記録(日付)
## 本日のTODO
* Module Managerについて
  * Module System
  * Module Manager
  * Module Class
* より詳しくLaminasのMVCを学ぶために[Documentation](https://docs.laminas.dev/laminas-mvc/intro/)にて学習

## 本日のTODOの達成率とその理由
* 本日のTODOの達成率:**100%**
* 理由
Module Managerにて`Module Manager Events`や`Module Manager Listeners`の種類が多く、読み込むのに時間がかかってしまいました。

## 本日学んだこと / 振り返り
* モジュールの構造として、MVC 機能を含む PHP コード、ライブラリコード、ビュースクリプト、 画像、CSS、JavaScript などのパブリックアセットなど、あらゆるものを含めることが可能みたいです。

* 決まり事として、モジュールがPHPの名前空間として機能すること。そしてその名前空間の下に大本の`Module`クラスを含むこと。

* 気をつけなければいけないこととして、モジュールは名前空間として機能するので、モジュール、ルート、ディレクトリはその名前空間出なければいけないということ。(チュートリアルでは`Album`で統一)

* モジュールクラスの場所ですが、autoload_*.phpファイルやComposerのオートローディング機能を使ってオートローダを定義している場合、Module.phpファイルをソースコードと一緒に配置することができる。(公式では、Composerでオートローディングを定義することを推奨。チュートリアルでもComposerでオートローディングを定義しました。)

## 困りごと / 質問など
* Laminasについての記事がほぼ無く、検索をかけるとZendの記事がヒットする時があります。同じメソッドではあり、Zendの後継フレームワークということですが、そちらは参考にしない方がよろしいのでしょうか...

## 明日のTODO
* LaminasのMVC学習
* Module Managerについての学習
  * Module Class
